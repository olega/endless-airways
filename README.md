# Read README before launch project


## To correct start launch project from scene called `MainScene`.

## TODO:

- [x] Add basic game logic
- [ ] Add some SFX for player moving, player death e.t.c.
- [ ] Add setting menu
- [ ] Add sprites for particles (move, take damage)
- [ ] Add change background parallax (day, evening, night, morning)
- [ ] Add animation for start gameplay (take-off)
