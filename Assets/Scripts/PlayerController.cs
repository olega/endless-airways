using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float yIncrement;
    public float speed;
    public float minHeight;
    public float maxHeight;
    public int health;
    public GameObject effect;
    public Text healthText;
    public Text healthTextShadow;
    public GameObject gameOverPanel;
    public GameObject scoreManager;
    public GameObject gameplayCanvas;


    private Vector2 _targetPos;

    void Update()
    {
        healthText.text = health.ToString();
        healthTextShadow.text = health.ToString();

        if (health <= 0)
        {
            gameOverPanel.SetActive(true);
            Destroy(gameObject);
            scoreManager.gameObject.SetActive(false);
            gameplayCanvas.gameObject.SetActive(false);
        }
        transform.position = Vector2.MoveTowards(transform.position, _targetPos, speed * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.W) && transform.position.y < maxHeight)
        {
            Instantiate(effect, transform.position, Quaternion.identity);
            _targetPos = new Vector2(transform.position.x, transform.position.y + yIncrement);
        }
        if (Input.GetKeyDown(KeyCode.S)&& transform.position.y > minHeight)
        {
            Instantiate(effect, transform.position, Quaternion.identity);
            _targetPos = new Vector2(transform.position.x, transform.position.y - yIncrement);
        }
    }
}
