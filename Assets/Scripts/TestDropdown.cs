using UnityEngine;
using UnityEngine.UI;

public class TestDropdown : MonoBehaviour
{
    public Dropdown dropdown;

    public GameObject bgDay;
    public GameObject bgEvening;
    public GameObject bgNight;

    public void Click()
    {
        int index = dropdown.value;
        switch (index)
        {
            case 0:
                ClickIfSelectedDay();
                break;

            case 1: 
                ClickIfSelectedEvening();
                break;
            
            case 2: 
                ClickIfSelectedNight();
                break;
        }
    }

    public void ClickIfSelectedDay()
    {
        bgDay.SetActive(true);
        bgEvening.SetActive(false);
        bgNight.SetActive(false);
    }

    public void ClickIfSelectedEvening()
    {
        bgDay.SetActive(false);
        bgEvening.SetActive(true);
        bgNight.SetActive(false);
    }
    
    public void ClickIfSelectedNight()
    {
        bgDay.SetActive(false);
        bgEvening.SetActive(false);
        bgNight.SetActive(true);
    }
}
