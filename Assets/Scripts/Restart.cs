using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Restart : MonoBehaviour
{
    public Text score;
    public Text scoreShadow;
    public ScoreManager sm;

    
    
    void Start()
    {
        score.text = sm.score.ToString();
        scoreShadow.text = sm.score.ToString();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
