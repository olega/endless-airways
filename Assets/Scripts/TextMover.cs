using DG.Tweening;
using UnityEngine;

public class TextMover : MonoBehaviour
{
    public Vector3 to;
    public float duration;
    
    void Update()
    {
        //transform.DOMove(endValue, duration);
        transform.DOMove(to, duration);
    }
}
