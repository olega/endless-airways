using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BackgroundFader : MonoBehaviour
{
    public Renderer spriteMy;

    public int endValue;
    public float duration;
    
    //[Range(0, 1)] public float moveProgress;
    //public float moveSpeed;

    void Update()
    {
        spriteMy.material.DOFade(endValue, duration);
        //moveProgress = Mathf.PingPong(Time.time * moveSpeed, 1);
        //var materialColor = spriteMy.material.color;
        //materialColor.a = 255 * moveProgress; 
    }

}
