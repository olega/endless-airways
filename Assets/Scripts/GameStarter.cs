using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStarter : MonoBehaviour
{
    public GameObject titleCanvas;
    public GameObject gameplayCanvas;
    public GameObject gameplayObjects;
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            titleCanvas.SetActive(false);
            gameplayCanvas.SetActive(true);
            gameplayObjects.SetActive(true);
        }
    }
}
