using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class ObstacleSpawnerController : MonoBehaviour
{
    public GameObject[] obstaclePrefabVariants;
    public float startTimeBtwSpawn;
    public float decreaseTime;
    public float minTime;
    
    private float _timeBtwSpawn;

    private void Update()
    {
        if (_timeBtwSpawn <= 0)
        {
            int rand = Random.Range(0, obstaclePrefabVariants.Length);
            Instantiate(obstaclePrefabVariants[rand], transform.position, quaternion.identity);
            _timeBtwSpawn = startTimeBtwSpawn;
            if (startTimeBtwSpawn > minTime)
            {
                startTimeBtwSpawn -= decreaseTime;
            }
        }
        else
        {
            _timeBtwSpawn -= Time.deltaTime;
        }
    }
}
