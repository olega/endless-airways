using UnityEngine;

public class ObstacleController : MonoBehaviour
{
    public int damage;
    public float speed;
    public GameObject effect;
    

    void Update()
    {
       transform.Translate(Vector2.left * speed); 
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Instantiate(effect, transform.position, Quaternion.identity);
            other.GetComponent<PlayerController>().health -= damage;
            Destroy(gameObject);
        }
    }
}
